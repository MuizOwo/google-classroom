import React, { Component } from 'react';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom'
import './App.css';
import Navbar from './Component/Header/Navbar';
import ReactCalendar from './Component/Calendar/Calendar'
import Home from './Component/Home/Home'
import Toodo from './Component/To-do/Toodo'
import AddTodo from './Component/To-do/AddTodo'
import Gclass from './Component/Classes/Classes'


class App extends Component{
  state={
    todoos:[
      {id:1, content:'Submit Your Assignment Before 4:00pm'}
    ]
  }
  addTodo=(todo)=>{
    todo.id=Math.random();
    let todoos=[...this.state.todoos, todo];
    this.setState({
      todoos:todoos
    })
  }
  deleteTodo=(id)=>{
    let todoos=this.state.todoos.filter(todo=>{
      return todo.id !== id
    });
    this.setState({
      todoos:todoos
    })
  }

  render(){
    return (
      <Router>
        <div className="App">
            <Navbar />
              <Switch>
                <Route exact path='/' component= {Home} />

                <Route path='/calendar' component={ReactCalendar} />

                <div className="todo">
                <Route path='/todo' component={Toodo}>
                <Toodo addTodo={this.addTodo}/>
                    <AddTodo  deleteTodo={this.deleteTodo} todoos={this.state.todoos} />                    
                </Route>
                </div>

                <Route path='/classes' component={Gclass} />
                
              </Switch>
        </div>
      </Router>
  
    );
  }
}

export default App;
