import React, { Component } from 'react'
import { Link } from 'react-router-dom'



class Navbar extends Component {
           render() {
                      return (
                                 <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
                                 <ul className="navbar-nav">

                                 <li className="nav-item active"><Link to='/'>
                                 <a className="nav-link" href="#">Google Classroom</a></Link>
                                 </li>

                                 <li className="nav-item"><Link to='/classes'>
                                 <a className="nav-link" href="#">Classes</a></Link>
                                 </li>

                                 <li className="nav-item"> <Link to='/calendar'>
                                 <a className="nav-link" href="#">Calender</a></Link>
                                 </li>
                                 

                                 <li className="nav-item"><Link to='/todo'>
                                 <a className="nav-link" href="#">To do</a></Link>
                                 </li>

                                 <li className="nav-item">
                                 <a className="nav-link" href="#">Settings</a>
                                 </li>

                                 </ul>
                                 </nav>


                      )
           }
}

export default Navbar
