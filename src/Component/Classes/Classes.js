import React, { Component } from 'react'
import './Class.css'

class Gclass extends Component {
           render() {
                      return (
                                 <div className='body'>
                                 <div className="C row row-cols-1 row-cols-md-3">
                                 <div className="col mb-4">
                                 <div className="card">
                                   <div className="A card-body">
                                     <h5 className="card-title">LM 1031, Semester 1, Section 6</h5>
                                     <p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                     <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                                   </div>
                                 </div>
                                 </div>

                                 <div className="col mb-4">
                                 <div className="card">
                                   <div className="A card-body">
                                     <h5 className="card-title">Card title</h5>
                                     <p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                     <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                                   </div>
                                 </div>
                                 </div>
                                
                               </div>
                               </div>
                      )
           }
}

export default Gclass
