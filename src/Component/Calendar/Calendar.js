import React, { useState } from 'react'
import Calendar from 'react-calendar'
import './Calendar.css'

const ReactCalendar=()=> {
           const {date,setState}= useState(new Date());

           const onChange = date =>{
                      setState(date)
           }
           return (
                      <div className='Cal'>
                         <Calendar onChange={onChange} value={date} />   
                      </div>
           )
}

export default ReactCalendar
