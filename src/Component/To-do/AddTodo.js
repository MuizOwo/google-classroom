import React from 'react'
import './Todo.css'

const AddTodo=({todoos,deleteTodo})=> {

     const todoList =todoos.length ?(
          todoos.map(todo=>{
               return(
                    <div key={todo.id}>
                         <span onClick={()=>{deleteTodo(todo.id)}}>
                              {todo.content}
                         </span>
                    </div>
               )
                    })
          ):(
               <p>Dont have todo left</p>
          )

           return (
                      <div className='F '>
                                   {todoList}          
                      </div>
                      
          )
}

export default AddTodo
