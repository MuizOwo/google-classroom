import React, { Component } from 'react'

 class Toodo extends Component {

     state={
          content:' '
     }
     todoChange= (e) =>{
          this.setState({
               content: e.target.value
          })
     }
     todoSubmit=(e)=>{
          e.preventDefault();
          this.props.addTodo(this.state);
          this.setState({
               content: ' '
          })
     }

     render() {
          return (
               <form className='form-group' onSubmit={this.todoSubmit}>
                    <label className="label">Add todo: </label>
                    <input  className="U form-control form-control-lg" type="text" value={this.state.content} onChange={this.todoChange} />
               </form>
          )
     }
}

export default Toodo
